# About this test

## About loading the photos

I load the and fetch the photos info one time only. This was a decision that I made.
Didn't made it lightly, I thought about it a lot. In a working situation, cause of my lack of experience
this would be definitely a question that I would ask to one of my Seniors.
I decided this way, because we are talking about a small gallery, which implies a limited
amount of photos, not something like a search that will fetch an enormous amount of photos,
where in that case, considering the dimension of amount of the photos should only be fetched
for page.

## Let's start with the cons

I'm aware of some issues here and there:

- On rare occasions some photos don't have the total width (I believe it's the ones that are in portrait mode)
- On resizing there's slightly noticeable flickering of the cards (I don't think it's a HUGE issue, after all people won't be watching the photos while resizing the window, but still might be something to improve)
- Depending on the person might be a con the last row will be centered and not align to left with the top cards
- The description card has some html tags and html encode symbols. (Using the angular sanitizer I could make them to be rendered as DOM elements. And a costume function I could decode the HTML symbols)
- Might have missed some more

## The strong's

But I also believe in some strong's

- It has some nice and simple animations
- Uses angular material components, making it nice and clean
- The frontend is independent of the backend
- It follows most of the good practices of webDevelopment (or at least I believe it so)
- If I didn't miss anything it should work major Browsers (Tested in Chrome and Firefox)

## Why didn't I corrected the wrongs?

The Idea that I had about this project was more to check my knowledge of Angular, webDevelopment overall and my code practices.
I think that what's done shows already a lot of those.

## After this...

No matter the result I would like to have some more detailed feedback on, what could be improved.
My experience as a web developer is limited and so I know I've much to improve, so I would really appreciate the feedback

#### Thank you for your time!
