import { Injectable } from '@angular/core';

/**
 * Code that isn't specific to any service or components and it can
 * be needed in different places of the application
 */
@Injectable({
  providedIn: 'root',
})
export class HelpersService {
  constructor() {}

  /**
   * Returns an array with 'length' numbers starting from 'startNumber'
   * @param length the length of the counting array
   * @param startNumber the starting number
   */
  getCountingArray(length: number, startNumber: number = 0) {
    const result: number[] = [];
    for (let index = 0; index < length; index++) {
      result[index] = index + startNumber;
    }
    return result;
  }
}
