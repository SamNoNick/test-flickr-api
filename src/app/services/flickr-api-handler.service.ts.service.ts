import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, forkJoin, of, EMPTY } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';
import { GalleryPhotoModel } from '../gallery/gallery.model';

/**
 * The purpose of this service is to handle the communication with the
 * backend. It works like an adapter, and it maps flickr API -> Gallery Models
 * and handles all the requests to the backend
 *
 * If for some reason the backend decides to change it's API or even frontend
 * changes the backend provider of it, this should be the only affected file
 */

interface FlickrSearchResponse {
  photos: {
    photo: FlickrSearchItem[];
  };
  stat: string;
}

interface FlickrSearchItem {
  farm: string;
  id: string;
  secret: string;
  server: string;
  title: string;
}

interface FlickrGetInfoResponse {
  photo: FlickrPhoto;
  stat: string;
}

interface FlickrPhoto {
  farm: string;
  id: string;
  secret: string;
  server: string;
  title: {
    _content: string;
  };
  description: {
    _content: string;
  };
}

@Injectable({
  providedIn: 'root',
})
export class FlickrApiHandler {
  apiUrl: string = 'https://www.flickr.com/services/rest/';
  key: string = '5d13e1bb4cd357ac69e89af168a8ff6a';
  secrete: string = '1ac7d12899c28b9d';

  constructor(private http: HttpClient) {}

  getPhotoById(photoId: string): Observable<GalleryPhotoModel> {
    const params = `?method=flickr.photos.getInfo&api_key=${this.key}&photo_id=${photoId}&format=json&nojsoncallback=1`;
    return this.http.get<FlickrGetInfoResponse>(this.apiUrl + params).pipe(
      map((response: FlickrGetInfoResponse) => {
        if (response.stat === 'ok') {
          const photo: FlickrPhoto = response.photo;
          return {
            title: photo.title._content,
            src: `https://farm${photo.farm}.staticflickr.com/${photo.server}/${photo.id}_${photo.secret}.jpg`,
            description: photo.description._content,
          } as GalleryPhotoModel;
        }
        // TODO: handle errors
        // here I'm not sure if this is the best approach, but I think
        return null;
      })
    );
  }

  getPhotosWithDescription(
    searchString: string,
    length: number
  ): Observable<GalleryPhotoModel[]> {
    return this.getPhotoSearchList(searchString, length).pipe(
      // get info for all of them with forkJoin and MergeMap
      mergeMap((items: FlickrSearchItem[]) => {
        if (items.length) {
          return forkJoin(
            items.map((item: FlickrSearchItem) => this.getPhotoById(item.id))
          );
        }
        return of([]);
      })
    );
  }

  private getPhotoSearchList(
    searchString: string,
    length: number
  ): Observable<FlickrSearchItem[]> {
    const params = `?method=flickr.photos.search&api_key=${this.key}&text=${searchString}&format=json&nojsoncallback=1&per_page=${length}&page=1`;
    return this.http.get(this.apiUrl + params).pipe(
      map((response: FlickrSearchResponse) => {
        if (response.stat === 'ok') {
          return response.photos.photo as FlickrSearchItem[];
        }
        // TODO: handle errors better
        return [];
      })
    );
  }
}
