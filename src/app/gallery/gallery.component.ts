import { trigger, transition, style, animate } from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';
import { FlickrApiHandler } from '../services/flickr-api-handler.service.ts.service';
import { HelpersService } from '../services/helpers.service';
import { fadeInUp } from './gallery.animations';
import { GalleryPhotoModel } from './gallery.model';

/**
 * I use the set and get with the Input tag because I believe it's better to use it then ngOnChanges.
 * On bigger and complex components if using only ngOnChanges it will be harder to
 * maintain code and it's readability. On the other hand the component has more overhead
 */
@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
  animations: [fadeInUp],
})
export class GalleryComponent implements OnInit {
  private _activePage: number = 0;
  private _photosPerPage: number = 1;

  allPhotos: GalleryPhotoModel[] = [];
  displayedPhotos: GalleryPhotoModel[] = [];

  constructor(
    private galleryApiHandler: FlickrApiHandler,
    private helpers: HelpersService
  ) {}

  @Input() set activePage(page: number) {
    if (page < 0) {
      throw new Error('Invalid page Number');
    }
    this._activePage = page;
    this.setDisplayedPhotos();
  }
  get activePage() {
    return this._activePage;
  }

  @Input() set photosPerPage(value: number) {
    if (value < 0) {
      throw new Error('Invalid page Number');
    }
    this._photosPerPage = value;
    this.setDisplayedPhotos();
  }
  get photosPerPage() {
    return this._photosPerPage;
  }

  ngOnInit(): void {
    this.galleryApiHandler
      .getPhotosWithDescription('dog', 100)
      .subscribe((photos: GalleryPhotoModel[]) => {
        this.allPhotos = photos;
        this.setDisplayedPhotos();
      });
  }

  private setDisplayedPhotos() {
    const result = [];
    this.helpers.getCountingArray(this.photosPerPage).forEach((i: number) => {
      let nextPhotoIndex = this.activePage * this.photosPerPage + i;
      if (this.allPhotos[nextPhotoIndex]) {
        result.push(this.allPhotos[nextPhotoIndex]);
      }
    });
    this.displayedPhotos = result;
  }
}
