import { animate, style, transition, trigger } from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';
import { fadeInUp } from '../gallery.animations';
import { GalleryPhotoModel } from '../gallery.model';

@Component({
  selector: 'app-gallery-card',
  templateUrl: './gallery-card.component.html',
  styleUrls: ['./gallery-card.component.scss'],
  animations: [fadeInUp],
})
export class GalleryCardComponent implements OnInit {
  private _photo: GalleryPhotoModel;

  @Input()
  set photo(photo: GalleryPhotoModel) {
    this._photo = photo;
  }
  get photo() {
    return this._photo;
  }

  ngOnInit(): void {}
}
