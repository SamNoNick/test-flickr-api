/**
 * This are the models of the gallery.
 * It's important to have a model that as no
 * dependencies from the Backend API.
 */
export interface GalleryPhotoModel {
  src: string;
  title: string;
  description?: string;
}
