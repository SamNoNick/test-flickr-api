import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { AngularMaterialModule } from '../shared/angular-material.module';
import { GalleryComponent } from './gallery.component';
import { GalleryCardComponent } from './gallery-card/gallery-card.component';

@NgModule({
  declarations: [GalleryComponent, GalleryCardComponent],
  imports: [CommonModule, HttpClientModule, AngularMaterialModule],
  exports: [GalleryComponent],
  providers: [],
})
export class GalleryModule {}
