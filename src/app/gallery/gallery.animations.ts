import { trigger, transition, style, animate } from '@angular/animations';

export const fadeInUp = trigger('fadeInUp', [
  transition(':enter', [
    style({
      opacity: 0,
      transform: 'translate(0, 20px)',
    }),
    animate('400ms cubic-bezier(0.35, 0, 0.25, 1)'),
  ]),
]);
